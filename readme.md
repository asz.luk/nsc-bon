# Voucher Generator
> The tool allows you to create a gift voucher with a unique QR code. In the tool you can check whether the voucher has been used and confirm it. The project was created to automate the sale and verification of gift vouchers. All you need is a scan of the QR code on the gift voucher.

## Table of contents.
* [Technologies](#technologies)
* [Screenshots](#screenshots)
* [Status](#status)

### Technologies
Project is created with:
* Symfony 5
* Doctrine
* PHP 7
* HTML / CSS

### Screenshots
![alt text](public/img/7.png "Screenshots")

![alt text](public/img/6.png "Screenshots")

![alt text](public/img/5.png "Screenshots")

![alt text](public/img/8.png "Screenshots")

![alt text](public/img/4.png "Screenshots")

![alt text](public/img/3.png "Screenshots")

![alt text](public/img/2.png "Screenshots")

![alt text](public/img/1.png "Screenshots")


### Status
The project is not fully finished. Things to do below.
- Exception handling.
- Logger.
- Views adapted to mobile devices.
- Own error pages.
- Tests.
- "Settings", "Logs" section.
- Docker.
- CI / CD.
