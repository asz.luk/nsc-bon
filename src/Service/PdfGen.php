<?php

namespace App\Service;

use Dompdf\Dompdf;
use Dompdf\Options;

class PdfGen
{
    /**
     * Generates and stores a voucher in pdf file.
     *
     * @param $name
     * @param $html
     *
     * @return void
     */
    public function make($name, $html, $template)
    {
        $options = new Options();

        if ($template == 'valentine') {
            $options->set('defaultFont', 'didot');
        } else {
            $options->set('defaultFont', 'parisienne');
        }

        $pdf = new Dompdf($options);
        $pdf->loadHtml($html);
        $pdf->setPaper('A4', 'portrait');
        $pdf->render();

        $output = $pdf->output();
        $pdfFilepath =  '../public/voucher/' . $name . '.pdf';
        file_put_contents($pdfFilepath, $output);
    }
}
