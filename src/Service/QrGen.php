<?php

namespace App\Service;

use Endroid\QrCode\QrCode;

class QrGen
{
    /**
     * Generates and stores a QR code.
     *
     * @param $url
     * @param $name
     *
     * @return void
     */
    public function make($url, $name)
    {
	$url = $url . $name;
        $code = new QrCode($url);
        $code->setSize(200);
        $code->writeFile('../public/codes/' . $name . '.png');
    }
}
