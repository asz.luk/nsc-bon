<?php

namespace App\Model;

use App\Entity\Voucher;
use Doctrine\ORM\EntityManagerInterface;

class VoucherFactory
{
    /**
     * Interface for managing entities.
     *
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * VoucherFactory constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Creates a voucher object and saves it to the database.
     *
     * @param $name
     * @param $user
     * @param $customer
     * @param $present
     *
     * @return void
     */
    public function save($name, $user, $customer, $present)
    {
        $voucher = new Voucher();
        $voucher->setGuid($name);
        $voucher->setDate(new \DateTime());
        $voucher->setUdate(NULL);
        $voucher->setAuthor($user);
        $voucher->setConfirmed(NULL);
        $voucher->setStatus(FALSE);
        $voucher->setCustomer($customer);
        $voucher->setPresent($present);

        $entityManager = $this->entityManager;
        $entityManager->persist($voucher);
        $entityManager->flush();
    }

    /**
     * Update voucher status.
     *
     * @param $voucher
     * @param $user
     *
     * @return void
     */
    public function update($voucher, $user)
    {
        $voucher->setStatus(True);
        $voucher->setUdate(new \DateTime());
        $voucher->setConfirmed($user);

        $entityManager = $this->entityManager;
        $entityManager->persist($voucher);
        $entityManager->flush();
    }
}
