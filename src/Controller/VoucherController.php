<?php

namespace App\Controller;

use App\Entity\Voucher;
use App\Model\VoucherFactory;
use App\Form\VoucherType;
use App\Form\VoucherSearchType;
use App\Service\QrGen;
use App\Service\PdfGen;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class VoucherController extends AbstractController
{
    /**
     * @Route("/dashboard", name="dashboard")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dashboard()
    {
        return $this->render('dashboard.html.twig');
    }

    /**
     * Creates a voucher in PDF.
     *
     * @Route("/make", name="make")
     *
     * @param Request $request
     * @param QrGen $code
     * @param PdfGen $pdf
     * @param VoucherFactory $voucher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function generate(Request $request, QrGen $code, PdfGen $pdf, VoucherFactory $voucher)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $user = $this->getUser()->getUsername();

        $form = $this->createForm(VoucherType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $name = uuid_create(UUID_TYPE_RANDOM);
            $code->make('http://bon.naturalskinclinic.pl/verify/', $name);
            
            if ($data['template'] == 'valentine') {
                $html = $this->renderView('valentine.html', [
                    'name' => $name,
                    'owner' => $data['name'],
                    'amount' => $data['amount']
                ]);
            } else {
                $html = $this->renderView('voucher.html', [
                    'name' => $name,
                    'owner' => $data['name'],
                    'amount' => $data['amount']
                ]);
            }

            $pdf->make($name, $html, $data['template']);
            $voucher->save($name, $user, $data['name'], $data['amount']);
            $file = $this->getParameter('kernel.project_dir') . "/public/voucher/" . $name . ".pdf";

            return new BinaryFileResponse($file);
        }

        return $this->render('gen.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Shows voucher information.
     *
     * @Route("/check", name="check")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function check(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $voucher = $this->getDoctrine()->getRepository(Voucher::class)->findLastVoucher();
        $form = $this->createForm(VoucherSearchType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $voucher = $this->getDoctrine()->getRepository(Voucher::class)->findVoucherByGuid($data['id']);

            return $this->render('check.html.twig', [
                'form' => $form->createView(),
                'voucher' => $voucher
            ]);
        }

        return $this->render('check.html.twig', [
            'form' => $form->createView(),
            'voucher' => $voucher
        ]);
    }

    /**
     * Verifies voucher status.
     *
     * @Route("/verify/{id}", name="/verify")
     *
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function verify($id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $voucher = $this->getDoctrine()->getRepository(Voucher::class)->findVoucherByGuid($id);

        if ($voucher->getStatus() == True) {
            return $this->render('confirmed.html.twig', [
                'id' => $id
            ]);
        } else {
            return $this->render('unconfirmed.html.twig', [
                'id' => $id
            ]);
        }
    }

    /**
     * Change voucher status.
     *
     * @Route("/lock/{id}", name="lock")
     *
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function lock(VoucherFactory $model, $id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $user = $this->getUser()->getUsername();

        $voucher = $this->getDoctrine()->getRepository(Voucher::class)->findVoucherByGuid($id);
        $model->update($voucher, $user);

        return $this->render('successful.html.twig');
    }
}
