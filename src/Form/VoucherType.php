<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class VoucherType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('template', ChoiceType::class, [
                'choices'  => [
                    'Default' => 'default',
                    'Valentine' => 'valentine'
                ],
            ])
            ->add('name', TextType::class)
            ->add('amount', TextType::class)
            ->add('save', SubmitType::class, ['label' => "Generate"]);
    }
}
